<!--
To replace with correct values:
- reinstall.sh
- SONARTOKEN
- VERSION

Emojis: https://www.webfx.com/tools/emoji-cheat-sheet/
Record terminal: https://asciinema.org/
-->

<div align="center">
	<img src="logo.png" width="30%" alt="project's logo" style='fill: #94d31b'>
</div>

---

<div align="center">

__The best way to reinstall a mac!__

</div>

<div align="center">

<img src='https://img.shields.io/badge/Built%20with-Shell-blue?style=for-the-badge'> <img src='https://img.shields.io/badge/Made%20with-LOVE-red?style=for-the-badge'>
</div>

# Table of content
[[_TOC_]]

# About reinstall.sh
## What is reinstall.sh
The `reinstall.sh` project is the solution to one of the worst OCDs a computer scientist can have: how to keep your machine shiny as a new penny (from a software point of view) when you hate digital clutter and soiling?

`reinstall.sh` is a shell script that allows, in a single line, to reinstall applications and settings on a Mac.

This script has allowed me, beyond saving me a lot of time, to learn the Bash language. It also served as a laboratory for me to experiment with certain subtleties of language.

## Installation
That's the beauty of single-file shell scripts: there is nothing to install!

## How to use it?
Fire a terminal, copy/paste this line:

```shell
zsh <(curl -s https://gitlab.com/X99/reinstall.sh/raw/master/reinstall.sh)
```

And hit enter. The script will ask for confirmation, your admin password (for some `sudo` commands). It will take a while to complete, from a few minutes to dozens depending on your connection speed and, of course, on the amount of software you install.

Here's an example output:
![Example output](example.png)

## Technical stuff/how to customize the script to *YOUR* needs

There are a few thing to know before you dive in. Although the script's not so complex, there a a few tricks here and there. In the next to sections I will show you `reinstall.sh` guts.

### The `run` function
```shell
run() {
    input="'$*'"
    if output=$( "$@" 2>&1 ); then
        printf "%s✔%s" "${green}${bold}" "${reset}" # discard output
    else
        printf "%s✘%s" "${red}${bold}" "${reset}" # discard output
        {
            printf "Error on line %s\n" "$(caller)"
            printf "Command: %s\n" "$input"
            printf "Error message: %s\n\n" "$output"
        } >> log.txt
    fi
}
```

The `run` function runs a command passed as its first parameter and:

- mutes its output (on both `stdout` and `stderr`)
- prints a green checkmark if everything went fine
- prints a red X if something wrong happened. `run` also logs what happened in `log.txt` file under this form:

```
Error on line 155 /dev/fd/11
Command: 'pm2 start /usr/local/bin/node-red -- -v'
Error message: [PM2][ERROR] Script already launched, add -f option to force re-execution
````

This allows you to debug comfortably the script.

### Installing lists of apps, packages, fonts...

There  are a few spots in the script where list of things are installed: apps, fonts, VSCode extensions and CLI tools.

In these cases the code is each time very similar: a list is declared, then iterated:
```shell
declare -a list=(
    "item1"
    "item2"
)
for item in "${list[@]}"; do
    printf "\n  Installing %s%s%s..." "${bold}" "${item}" "${reset}"
    run WHATEVER COMMAND NEEDED TO INSTALL THE ITEM
    done
```


## Credits

A big thank you to [r/bash](https://www.reddit.com/r/bash)'s gurus for their help, especially [u/whetu](https://www.reddit.com/user/whetu/), for their help!

## Licence
<div align="center">

![CC](https://mirrors.creativecommons.org/presskit/icons/cc.svg) ![BY](https://mirrors.creativecommons.org/presskit/icons/by.svg) ![SA](https://mirrors.creativecommons.org/presskit/icons/sa.svg)

</div>

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

## Support
If you find this code useful and appreciate my work, feel free to click the button below to donate and help me continue. Every cent is appreciated! Thank you! :ok_hand:
<div align="center">

[![](donate.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=6EX6SCMSYPF94&currency_code=EUR&source=url)

</div>


## Code quality

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=bugs'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=code_smells'/>
	<img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=ncloc'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=sqale_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=alert_status'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=reliability_rating'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=security_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=sqale_index'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=reinstall.sh&metric=vulnerabilities'/>
</div>


# Credits
Project logo made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

# About the author

- [Twitter](https://twitter.com/therealX99)
- [Linkedin](https://www.linkedin.com/in/pierre-vernaeckt/)

<div align="center">
	<a href='https://pierre.vernaeckt.fr'>
		<img width='250' src='https://static.x99.fr/ULm9SWsQ6gzupZ.svg'>
	</a>
</div>

